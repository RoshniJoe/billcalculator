//
//  BillItems.swift
//  BillEngine
//
//  Created by Roshni Varghese on 2020-02-02.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit

public struct BillItems {
    
    public let itemId: String
    public let itemCategoryId: String
    public let itemName: String
    public var itemAmount: Double
    public var isTaxExempted: Bool // items can be exempt from taxeso only certain category of items
    
    public init(itemId: String, itemCategoryId : String, itemName: String, itemAmount: Double, isTaxExempted: Bool) {
        self.itemId = itemId
        self.itemCategoryId = itemCategoryId
        self.itemName = itemName
        self.itemAmount = itemAmount
        self.isTaxExempted = isTaxExempted
    }
}
