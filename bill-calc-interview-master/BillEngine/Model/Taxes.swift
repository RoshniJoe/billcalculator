//
//  Taxes.swift
//  BillEngine
//
//  Created by Roshni Varghese on 2020-02-02.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit

public struct Taxes {
    
    public let taxId: String // uniquely identifiable
    public let categoryIds: [String]? // tax can apply to only certain category of items
    public let taxPercentage: Double
    public var isEnabled: Bool = false
    
    public init(taxId: String, categoryIds: [String]?, taxPercentage: Double) {
        self.taxId = taxId
        self.categoryIds = categoryIds
        self.taxPercentage = taxPercentage
    }
}
