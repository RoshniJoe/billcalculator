//
//  Discounts.swift
//  BillEngine
//
//  Created by Roshni Varghese on 2020-02-02.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit

public struct Discounts {
    
    public let discountId: String // uniquely identifiable
    public var orderApplied: Int // order of applied discounts matter and can change the final o/p totals
    public let discountType: DiscountTypes
    public var isEnabled: Bool = false
    
    public init(discountId: String, discountType: DiscountTypes, orderApplied: Int) {
        self.discountId = discountId
        self.orderApplied = orderApplied
        self.discountType = discountType
    }
    
//     MARK: 2 types of discounts

    public enum DiscountTypes {
        case dollarAmount(value : Double)
        case percentage(value : Double)
    }
}
