//
//  BillEngine.swift
//  BillEngine
//
//  Created by Roshni Varghese on 2020-02-02.
//  Copyright © 2020 Roshni Varghese. All rights reserved.
//

import UIKit

public struct BillData {
    public var subtotal: Double
    public var discounts: Double
    public var tax: Double
    public var total: Double
    
    public init(subtotal: Double = 0.0, discounts: Double = 0.0, tax: Double = 0.0, total: Double = 0.0) {
        self.subtotal = subtotal
        self.discounts = discounts
        self.tax = tax
        self.total = total
    }
}

public class BillEngine {
    
    public static let shared = BillEngine()
    
    private var billItems: [BillItems] = []
    private var discounts: [Discounts] = []
    private var taxes: [Taxes] = []
    
    //MARK: Discount Calculations
    
    // A bill can have multiple discounts applied to it
    
    public func addDiscounts(discounts: [Discounts]) {
        self.discounts += discounts
        orderedDiscounts()
    }
    
    // The order of applied discounts matter and can change the final o/p totals
    
    public func orderOfAppliedDiscounts(discountId : String, order: Int) {
        for (_, var discount) in self.discounts.enumerated() {
            discount.orderApplied = discount.discountId == discountId ? order : 0
        }
        orderedDiscounts()
    }
    
    // sort items according to order
    
    public func orderedDiscounts(){
        self.discounts = self.discounts.sorted {
            $0.orderApplied < $1.orderApplied
        }
    }
    
    public func applyDiscounts(discountId: String) {
        for (_, var discount) in self.discounts.enumerated() {
            discount.isEnabled = discount.discountId == discountId ? true : false
        }
    }
    
    // Modify discounts on removing an item
    
    public func removeDiscount(discountIdToBeRemoved: String) {
        self.discounts = self.discounts.filter({ (discount) -> Bool in
            discount.discountId != discountIdToBeRemoved
        })
    }
    
    //MARK: Tax Calculations
    
    // A bill can have multiple taxes applied to it
    
    public func addTaxes(taxes: [Taxes]) {
        self.taxes += taxes
    }
    
    public func applyTaxes(taxId: String) {
        for (_, var item) in self.taxes.enumerated() {
            item.isEnabled = item.taxId == taxId ? true : false
        }
    }
    
    // Modify taxes on removing an item
    
    public func removeTax(taxIdToBeRemoved: String) {
        self.taxes = self.taxes.filter({ (tax) -> Bool in
            tax.taxId != taxIdToBeRemoved
        })
    }
    
    //MARK: Total Amount Calculations
    
    public func addBillItem(billItem: BillItems) -> BillData {
        self.billItems.append(billItem)
        return getBillData()
    }
    
    // Items can be exempted from tax
    
    public func isBillItemTaxExempted(taxExempted: Bool, itemId: String) {
        for (_, var item) in self.billItems.enumerated() {
            item.isTaxExempted = item.itemId == itemId ? taxExempted : false
        }
    }
    
    public func removeBillItem(itemId: String) -> BillData {
        self.billItems = self.billItems.filter({ (item) -> Bool in
            item.itemId != itemId
        })
        return self.getBillData()
    }
    
    public func getBillData() -> BillData {
        let subtotal = self.calulateSubtotal()
        let discount = self.calculateDiscounts()
        let tax = self.calculateTax()
        let total = subtotal + tax - discount
        
        let billData = BillData.init(subtotal: subtotal, discounts: discount, tax: tax, total: total)
        
        return billData
    }
    
    // Calculate Bill Data to be displayed
    
    public func calulateSubtotal() -> Double {
        let subtotal = self.billItems.reduce(0.0) { (result: Double, billItem) -> Double in
            result + billItem.itemAmount
        }
        return subtotal
    }
    
    public func calculateTax() -> Double {
        var taxAppliedCategories = [String]()
        for (_, tax) in self.taxes.enumerated() {
            if let taxCategoryIds = tax.categoryIds, tax.isEnabled  {
                taxAppliedCategories.append(contentsOf: taxCategoryIds)
            }
        }
        taxAppliedCategories = Array(Set(taxAppliedCategories))
        
        var calculatedtax = 0.0
        
        for (_, item) in self.billItems.enumerated() {
            if !item.isTaxExempted && taxAppliedCategories.contains(item.itemCategoryId) {
                for (_, tax) in self.taxes.enumerated() {
                    if tax.isEnabled {
                        if let taxCategoryIds = tax.categoryIds {
                            if taxCategoryIds.contains(item.itemCategoryId) {
                                calculatedtax += item.itemAmount * tax.taxPercentage / 100
                            }
                        }else {
                            calculatedtax += item.itemAmount * tax.taxPercentage / 100
                        }
                    }
                }
            }
        }
        return calculatedtax
    }
    
    public func calculateDiscounts() -> Double {
        let discountedItems = self.billItems
        for (_, discount) in self.discounts.enumerated() where discount.isEnabled {
            switch discount.discountType {
            case .dollarAmount(let value):
                let subtotal = self.billItems.reduce(0.0) { (result: Double, billItem: BillItems) -> Double in
                    result + billItem.itemAmount
                }
                for (_, var item) in discountedItems.enumerated() {
                    let discountedAmount = value * item.itemAmount / subtotal
                    item.itemAmount -= discountedAmount
                }
            case .percentage(let value):
                for (_, var item) in discountedItems.enumerated() {
                    let discountedAmount = item.itemAmount * value / 100
                    item.itemAmount -= discountedAmount
                }
            }
        }
        let totalDiscount = discountedItems.reduce(0.0) { (result: Double, item) -> Double in
            return result + item.itemAmount
        }
        return self.calulateSubtotal() + self.calculateTax() - totalDiscount
    }
}
