//
//  RegisterViewController.swift
//  POS
//
//  Created by Tayson Nguyen on 2019-04-23.
//  Copyright © 2019 TouchBistro. All rights reserved.
//

import UIKit
import BillEngine

class RegisterViewController: UIViewController {
    let cellIdentifier = "Cell"
    
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var orderTableView: UITableView!
    
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var discountsLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    let viewModel = RegisterViewModel()
    
    let billEngine = BillEngine.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTableView.dataSource = self
        orderTableView.dataSource = self
        menuTableView.delegate = self
        orderTableView.delegate = self
        
        addDiscountsAndTaxes()
    }
    
    @IBAction func showTaxes() {
        let rootVC = TaxViewController(style: .grouped)
        rootVC.delegate = self
        let vc = UINavigationController(rootViewController: rootVC)
        vc.modalPresentationStyle = .formSheet
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func showDiscounts() {
        let rootVC = DiscountViewController(style: .grouped)
        rootVC.delegate = self
        let vc = UINavigationController(rootViewController: rootVC)
        vc.modalPresentationStyle = .formSheet
        present(vc, animated: true, completion: nil)
    }

    private func addDiscountsAndTaxes() {
        let discount5DollarsValue = Discounts.DiscountTypes.dollarAmount(value: 5)
        let discount5Dollars = Discounts(discountId: "$5.00", discountType: discount5DollarsValue, orderApplied: 1)
        let discount10PercentValue = Discounts.DiscountTypes.percentage(value: 10)
        let discount10Percent = Discounts(discountId: "10%", discountType: discount10PercentValue, orderApplied: 2)
        let discount20PercentValue = Discounts.DiscountTypes.percentage(value: 20)
        let discount20Percent = Discounts(discountId: "20%", discountType: discount20PercentValue, orderApplied: 3)
        billEngine.addDiscounts(discounts: [discount5Dollars, discount10Percent, discount20Percent])

        let tax1 = Taxes(taxId: "Tax 1 (5%)", categoryIds: ["Appetizers"], taxPercentage: 5)
        let tax2 = Taxes(taxId: "Tax 2 (8%)", categoryIds: ["Mains"], taxPercentage: 8)
        let alcoholTax = Taxes(taxId: "Alcohol Tax (10%)", categoryIds: ["Alcohol"], taxPercentage: 10)
        billEngine.addTaxes(taxes: [tax1, tax2, alcoholTax])
        
        billEngine.enableTaxes(taxId: "Tax 1 (5%)")
        billEngine.enableTaxes(taxId: "Tax 2 (8%)")
        billEngine.enableTaxes(taxId: "Alcohol Tax (10%)")
    }
    
    func calculateBillData() {
        let billData = billEngine.getBillData()
        print(billData)
        self.subtotalLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.subtotal))
        self.discountsLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.discounts))
        self.taxLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.tax))
        self.totalLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.total))
    }
}

extension RegisterViewController: TaxesSelectedDelegate {
    
    func taxesSelected() {
        for tax in taxs {
            let taxId = tax.label
            if tax.isEnabled {
                billEngine.enableTaxes(taxId: taxId)
            }else {
                billEngine.disableTaxes(taxId: taxId)
            }
        }
        self.calculateBillData()
    }
}

extension RegisterViewController: DiscountsSelectedDelegate {
    func discountsSelected() {
        for discount in discouns {
            let discountId = discount.label
            if discount.isEnabled {
                billEngine.enableDiscounts(discountId: discountId)
            }else {
                billEngine.disableDiscounts(discountId: discountId)
            }
        }
        self.calculateBillData()
    }
}

extension RegisterViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == menuTableView {
            return viewModel.menuCategoryTitle(in: section)
            
        } else if tableView == orderTableView {
            return viewModel.orderTitle(in: section)
        }
        
        fatalError()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == menuTableView {
            return viewModel.numberOfMenuCategories()
        } else if tableView == orderTableView {
            return 1
        }
        
        fatalError()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == menuTableView {
            return viewModel.numberOfMenuItems(in: section)
            
        } else if tableView == orderTableView {
            return viewModel.numberOfOrderItems(in: section)
        }
        
        fatalError()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) ?? UITableViewCell(style: .value1, reuseIdentifier: cellIdentifier)
        
        if tableView == menuTableView {
            cell.textLabel?.text = viewModel.menuItemName(at: indexPath)
            cell.detailTextLabel?.text = viewModel.menuItemPrice(at: indexPath)
            
        } else if tableView == orderTableView {
            cell.textLabel?.text = viewModel.labelForOrderItem(at: indexPath)
            cell.detailTextLabel?.text = viewModel.orderItemPrice(at: indexPath)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == menuTableView {
            let indexPaths = [viewModel.addItemToOrder(at: indexPath)]
            orderTableView.insertRows(at: indexPaths, with: .automatic)
            // calculate bill totals
            let item = categories[indexPath.section].items[indexPath.row]
            
            let billItem = BillItems(itemId: item.name, itemCategoryId: item.category, itemName: item.name, itemAmount: Double(truncating: item.price), isTaxExempted: item.isTaxExempt)
            let billData = billEngine.addBillItem(billItem: billItem)
            updateBillLabel(billData: billData)
        
        } else if tableView == orderTableView {
            viewModel.toggleTaxForOrderItem(at: indexPath)
            
            // recalculate bill
             let orderItems = viewModel.orderItems
             let itemId = orderItems[indexPath.row].name
             let isItemTaxExempted = orderItems[indexPath.row].isTaxExempt
             billEngine.isBillItemTaxExempted(taxExempted: isItemTaxExempted, itemId: itemId)
             self.calculateBillData()
            
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if tableView == menuTableView {
            return .none
        } else if tableView == orderTableView {
            return .delete
        }
        
        fatalError()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == orderTableView && editingStyle == .delete {
            // item before deleting it
            let itemToBeDeleted = viewModel.orderItems[indexPath.row]
            viewModel.removeItemFromOrder(at: indexPath)
            orderTableView.deleteRows(at: [indexPath], with: .automatic)
            // calculate bill total
            let itemId = itemToBeDeleted.name
            let billData = billEngine.removeBillItem(itemId: itemId)
            updateBillLabel(billData: billData)
        }
    }
    
    func updateBillLabel(billData: BillData) {
        print(billData)
        self.subtotalLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.subtotal))
        self.discountsLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.discounts))
        self.taxLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.tax))
        self.totalLabel.text = viewModel.formatter.string(from: NSDecimalNumber(value: billData.total))
    }
}


class RegisterViewModel {
    let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter
    }()
    
    var orderItems: [Item] = []
    
    func menuCategoryTitle(in section: Int) -> String? {
        return categories[section].name
    }
    
    func orderTitle(in section: Int) -> String? {
        return "Bill"
    }
    
    func numberOfMenuCategories() -> Int {
        return categories.count
    }
    
    func numberOfMenuItems(in section: Int) -> Int {
        return categories[section].items.count
    }
    
    func numberOfOrderItems(in section: Int) -> Int {
        return orderItems.count
    }
    
    func menuItemName(at indexPath: IndexPath) -> String? {
        return categories[indexPath.section].items[indexPath.row].name
    }
    
    func menuItemPrice(at indexPath: IndexPath) -> String? {
        let price = categories[indexPath.section].items[indexPath.row].price
        return formatter.string(from: price)
    }
    
    func labelForOrderItem(at indexPath: IndexPath) -> String? {
        let item = orderItems[indexPath.row]
       
        if item.isTaxExempt {
            return "\(item.name) (No Tax)"
        } else {
            return item.name
        }
    }
    
    func orderItemPrice(at indexPath: IndexPath) -> String? {
        let price = orderItems[indexPath.row].price
        return formatter.string(from: price)
    }
    
    func addItemToOrder(at indexPath: IndexPath) -> IndexPath {
        let item = categories[indexPath.section].items[indexPath.row]
        orderItems.append(item)
        return IndexPath(row: orderItems.count - 1, section: 0)
    }
    
    func removeItemFromOrder(at indexPath: IndexPath) {
        orderItems.remove(at: indexPath.row)
    }
    
    func toggleTaxForOrderItem(at indexPath: IndexPath) {
        orderItems[indexPath.row].isTaxExempt = !orderItems[indexPath.row].isTaxExempt
    }
}
